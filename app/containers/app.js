import React from 'react';
import ReactNative from 'react-native';

const {
  StyleSheet,
  Navigator,
  StatusBar,
  BackAndroid,
  View,
  Dimensions,
  InteractionManager,
  AppState,
  Platform,
} = ReactNative;

import {connect} from 'react-redux';
import Splash from '../pages/splash';
import {NaviGoBack} from '../utils/CommonUtils';
import JPush, {JpushEventReceiveCustomMessage, JpushEventReceiveMessage, JpushEventOpenMessage} from 'react-native-jpush';
import TaskListContainer from '../containers/TaskListContainer';
import GesturePassword from '../pages/gesturePassword';
import {TASKLIST_URL} from '../constants/Network';

var _navigator, _route;
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
var isPushNotice = false;

class App extends React.Component {
  constructor(props) {
    super(props);
    this.renderScene = this.renderScene.bind(this);
    this.goBack = this.goBack.bind(this);
    BackAndroid.addEventListener('hardwareBackPress', this.goBack);
  }

  // 这边几个都是React组件在加载时的特定的生命周期 https://fraserxu.me/2014/08/31/react-component-lifecycle/
  // 里面是一些推送用的方法，具体https://github.com/jpush/jpush-react-native
  componentDidMount() {
    //JPush.setAlias('');
    JPush.requestPermissions()
    this.pushlisteners = [
        JPush.addEventListener(JpushEventReceiveMessage, this.onReceiveMessage.bind(this)),
        JPush.addEventListener(JpushEventOpenMessage, this.onOpenMessage.bind(this)),
    ];
    JPush.setLatestNotificationNumber(1);
    AppState.addEventListener('change', this.handleAppStateChange);
  }

  componentWillUnmount() {
    this.pushlisteners.forEach(listener=> {
        JPush.removeEventListener(listener);
    });
    AppState.removeEventListener('change', this.handleAppStateChange);
  }

  handleAppStateChange() {
      if(AppState.currentState === 'active'){
        if(isPushNotice){
          isPushNotice = false;
          return;
        }
        if(_route.name != 'gesturePassword' && _route.name != 'Login' ){
          _navigator.push({
            name: 'gesturePassword',
            component: GesturePassword,
          });
        }
      }
  }

  goBack() {
    const {timeConsuming} = this.props;
    if (!timeConsuming.canGoBack || _route.name === 'gesturePassword') {
      return true;
    }
    return NaviGoBack(_navigator);
  }

  renderScene(route, navigator) {
    let Component = route.component; // 从initial Route 传递过来的
    _navigator = navigator;// navigator是一个Navigator的对象
    _route = route;
    return (
      <Component navigator={navigator} route={route} /> // 然后navigator作为props传递给了这个component。在Splash中我们可以直接拿到这个props.navigator:
    );
  }

  configureScene(route, routeStack) {
    return Navigator.SceneConfigs.FadeAndroid;//这个是页面之间跳转时候的动画
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <Navigator
          ref='navigator'
          style={styles.navigator}
          configureScene={this.configureScene} //可以通过configureScene属性获取指定路由对象的配置信息，从而改变场景的动画或者手势。
          renderScene={this.renderScene} //利用renderScene方法，导航栏可以根据指定的路由来渲染场景。
          initialRoute={{ // 这个指定了默认的页面，也就是启动app之后会看到界面的第一屏。
            component: Splash,
            name: 'Splash'
          }}/>
      </View>
    );
  }

  onReceiveMessage(message) {
  }

  onOpenMessage(message) {
    const {login} = this.props;
    if(login.rawData && login.rawData.userId && login.rawData.userId != '' && login.username && login.username!= ''){
      isPushNotice = true;
      JPush.clearAllNotifications();
      if(AppState.currentState == 'active'){
        if(_route.name === 'TaskListContainer'){
          _navigator.replaceAtIndex({
            name: "TaskListContainer",
            component: TaskListContainer,
            url: TASKLIST_URL,
            navBarTitle: "待办",}, 0);
        } else {
          InteractionManager.runAfterInteractions(() => {
            _navigator.push({
              name: "TaskListContainer",
              component: TaskListContainer,
              url: TASKLIST_URL,
              navBarTitle: "待办",
            });
          });
        }
      } else if (AppState.currentState == 'background'){
        _navigator.push({
          name: 'gesturePassword',
          component: GesturePassword,
          type: 'notice',
        });
      }
    }
  }
}

let styles = StyleSheet.create({
  navigator: {
    flex: 1
  },
});

function mapStateToProps(state) {
  const {timeConsuming, login} = state;
  return {
    timeConsuming,
    login
  }
}

export default connect(mapStateToProps)(App);
